<?php
require '../wp-load.php';
$postID = $_POST['idpostmanager'];
$domain = $_POST['domain'];

if(!empty($postID)) {
		$json = file_get_contents('https://'.$domain.'/wp-json/wp/v2/posts/'.$postID.'?_embed');
		$obj = json_decode($json,TRUE);
		$title = $obj['title']["rendered"];
        $slug =  $obj['slug'];
        $content =  $obj['content']["rendered"];
        $excerpt =  $obj['excerpt']["rendered"];
        $datepublished = $obj['date'];
        $category = 5728;
        $tags = 'exclude';
        $videod = $obj['videod'];
        $videord = $obj['videord'];
        $image = $obj['imgft_field'];
        if( !post_exists_by_slug( $slug ) ) {
            $post_id = wp_insert_post(
                array(
                    'comment_status'    =>   'closed',
                    'ping_status'       =>   'closed',
                    'post_author'       =>   1,
                    'post_title'        =>   $title,
                    'post_content'      =>   $content,
                    'post_excerpt'      =>   $excerpt,
                    'post_status'       =>   'draft',
                    'post_type'         =>   'post',
                    'post_category'     =>   array($category),
                    'tags_input'        =>   array('exclude','OIM')
                )
            );
            if(!empty($image)){
                generate_geatured_image($image,$post_id);
            }
            if(!empty($videod)){
                update_post_meta( $post_id, 'video_destacado', $videod );
            }
            if(!empty($videord)){
                update_post_meta( $post_id, 'video_recomendado', $videord );
            }
            $salidaurl = get_edit_post_link($post_id);
            $urlpreview = get_preview_post_link($post_id);
            if(!empty($salidaurl)){
                $salida = array('id'=>$post_id,'url'=>$salidaurl,'urlp'=>$urlpreview);
                header('Content-Type: application/json');
                $response = json_encode($salida);
                echo $response; 
            }
            
       	} else {
     		$post_id = -2;
        }
       	
    } 
 /**
 * post_exists_by_slug.
 *
 * @return mixed boolean false if no post exists; post ID otherwise.
 */
function post_exists_by_slug( $post_slug ) {
    $args_posts = array(
        'post_type'      => 'post',
        'post_status'    => 'any',
        'name'           => $post_slug,
        'posts_per_page' => 1,
    );
    $loop_posts = new WP_Query( $args_posts );
    if ( ! $loop_posts->have_posts() ) {
        return false;
    } else {
        $loop_posts->the_post();
        return $loop_posts->post->ID;
    }
}
function generate_geatured_image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}


?>
