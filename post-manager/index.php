<?php
require '../wp-load.php';
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Ole Interactive - PostManager</title>
<!-- Compiled and minified CSS -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@100;400;700&display=swap" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<style type="text/css">
	body { padding: 3rem 0; margin: 0px; font-family: 'Noto Sans JP', sans-serif; background-color: #f7f7f7; }
		div.wrap { width: 100%; max-width: 1000px; margin:0px auto; color: #212529; padding: 3rem!important;background-color: #fefefe; border-color: #fdfdfe; border-radius: .25rem;}
		.actionCreate{ color:#607d8b; }
		.actionCreate:hover{ color:#e53935; }
		.processing{ opacity: 0.5; background-image: url(assets/loader.gif); min-height: 300px; background-size: 50px; background-repeat: no-repeat; background-position: center;}
		.msg{ display: none; text-align: center; line-height: 50px; border: 2px solid; }
		.msg.success{background-color: #BFFFCF; border-color: #4ab866;}
		.msg.fail{background-color: #FFCFBF; border-color: #FF2626;}
		h1{ font-size: 30px;font-weight: 700; }
		h3{ font-weight: 100; font-size: 22px; }
		h4{ font-weight: 100; font-size: 18px; }
</style>
</head>

<body>
<div class="wrap">
	<header><h1>Post Manager</h1></header>
	<main>
		<div class="section">
			<div class="row">
				<div class="content blue-grey-text darken-3">
				<?php
				if( is_user_logged_in() ) { ?>
					<h5>Fuente de datos</h5>
					<p class="grey-text text-darken-2">Seleccionar el post a importar</p>
					<div class="msg"></div>
					<div class="content_posts">
						<ul id="postmanage-list" class="collection with-header">
						</ul>
					</div>
				<?php }else{ ?>
					<h4>No está autorizado</h4>
					<p class="grey-text text-darken-2">Se requiere un usuario válido para acceder</p>
				<?php } ?>

				</div>
			</div>
		</div>
	</main>
</div>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/behavior.js"></script>
</body>
</html>

