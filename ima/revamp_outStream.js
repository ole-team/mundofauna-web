class Revamp{
	constructor(type,creative,mind,close,position){
		this.branding = '<a href="https://www.revamp.pro/" target="_blank"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAAaCAYAAAAg0tunAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA+5JREFUeNrsWD1oU1EUPvclsQjavi4uHay4OqRbcUmqiyDUZNDBwbaLbjbBoYuQZFSQ/uCeBqygoEnBwaHQV1w62Ti4CY3goAg21aR/NO96bvq99Bpe8vqjVO07cLjv3Z93z/3ud8497wrypSGjz7+aG4HacCVUM6uhGlWDtfJGwJ5+c+l8udWYoA/bjtx/8s1ct+0lCohep06y1oQc4qKv1TjDhw5ASJESRL2GpGl+HWDw4lJQiQSF243zGbjLpKgqA1IkZwZ76i57eW55fA/jfHmQ/d4rpAizFh9fP1MH7+rrj/xOXE+WD6C31NnHLtwASzTqxILvwh4yNtKp4t50U0yM1AH0GXgwAQPLnML4AO5Xbrz6FGYATYPas++vArC/v182vc+zRo+EfRLxT7aPf/91DJQzt0yqnMpS5XSMqieJNjuItkOcGQeIbOaXhO7KgJi4a+nxj5Po8YvWh/G1YI3WgjatB2q0weWmwRqwaduQ00HeZZXrqOw7xlpijS8uLha5PsvPwyoOsCYxSS+3pbktrer5eQLjF/CNFPqp/kW8q0Q0x9rFmsD34mjP4wRs5SopxUTHLnwvx/MWdBtcR3ZszVNV8tyyiDnbsw7gIf4RebuvWtdwEA/KEAEwslzmsLButKtFDLCqdmX4KIyaACgLWNw59M9jwTGUqm8W34vhOzmA3o2NcnNXx64svv+OVf1aFWBD3JV9c1eitGqHybAL4t7D+H7Z+3Swx3PMifdvFQ4pJwZOopwFACZ2Wi3AaopNaWd38KzvcB4LLTr9FVs0cPIAoNw0x0QLOx27cgBepRoxnjcBcN1ZYthhlAt/OlQEtUTSAnjO4iMAyfkXLGk7n8S7es44bIH7lzFOZ1QJYA40bUTEpW9zgms5JY8vcd8CNinTZl3mDhUNUz4ai3rFQN19b7/8HK4Gd25jKir2hVT8c4mB0h5Svu4AOMqGRQDgCIy+xnVLcLMMjJ+FuxUAtGJCAW1qzDKXzgboC1T9h7htGWAWwa4hbQ7XGybNrngTGwutTxBh1UGuGSktLnukfY0TON/GnmbJCATpDBhUchikse+XOo9URE1sqkOoTbtiYkmri7Z0RRcb+D0P9x1pewo/uxmj1a4w/eikFgw8y1onAzOwEfPuvPiSroS2yYuBWwFpbVzos4403zpArqgOuCVW89BpTnIyIRNTkjVxqBiox6V/QDI6ew8pEdpbuuJLCwauMPtWfsM94rEEz0nVLB/AQ9z/4QdgB9TEVBoxcdgH0Pu2wC3+HSgmHmsGcvpSBPtM1JW4bl+H1LG+kWbg5uHGES1JJx9Abx9O4kIjqsXDAi5H9iU/BRgAF+vJWVJ7odwAAAAASUVORK5CYII=" style="width:80px; position: absolute; bottom:0px; right: 0px;"/></a>';
		this.adsManager;
		this.adsLoader;
		this.adDisplayContainer;
		this.intervalTimer;
		this.playButton;
		this.videoContent;
    this.adsInitialized;
    this.autoplayAllowed;
    this.autoplayRequiresMuted;
	}
}

Revamp.prototype.init = function() {
	var imaSDK = document.createElement('script');
	imaSDK.type = 'text/javascript';
	imaSDK.src = '//imasdk.googleapis.com/js/sdkloader/ima3.js';    
	document.getElementsByTagName('head')[0].appendChild(imaSDK);
	if (window.top.document.readyState !== "loading") {
		document.addEventListener("DOMContentLoaded", (event)=> {
			setTimeout(()=>{this.displayRevamp();}, 2000);
		});
	}else{
		document.addEventListener("DOMContentLoaded", (event)=> {
			setTimeout(()=>{this.displayRevamp();}, 2000);
		});
	}
	
	
};

Revamp.prototype.displayRevamp = function(){
	var wrapOutstream = document.createElement("div");
	wrapOutstream.setAttribute("class","wrap-revamp-oustream"); 
	wrapOutstream.style.cssText = 'position:relative; margin:15px auto; width:400px; height: 300px;';
	var containerOutstream = document.createElement("div");
	containerOutstream.setAttribute("id","mainContainerRevamp");
	containerOutstream.style.cssText = "position: relative; width: 400px; height: 300px;";
	var contentOustream = document.createElement("div");
	contentOustream.setAttribute("id","contentRevamp");
	contentOustream.style.cssText = "position: absolute; top: 0px; left: 0px; width: 400px; height: 300px;";
	contentOustream.innerHTML = '<video id="contentElementRevamp" playsinline muted style="width: 400px; height: 300px; overflow: hidden;"> <source src="https://storage.googleapis.com/gvabox/media/samples/stock.mp4"></source> </video>';
	var adOutstream =  document.createElement("div");
	adOutstream.setAttribute("id","adContainerRevamp");
	adOutstream.style.cssText = "position: absolute; top: 0px; left: 0px; width: 400px; height: 300px;";
	contentOustream.appendChild(adOutstream);
	containerOutstream.appendChild(contentOustream);
	wrapOutstream.appendChild(containerOutstream);
	//wrapOutstream.appendChild(this.branding);
	var closeRevamp = document.createElement("div");
	this.close = true;
	if(this.close !== false){
		closeRevamp.setAttribute("id","close_event_revamp"); 
		closeRevamp.style.cssText = 'position: absolute; top: 0px; right: 5px; z-index: 9999; cursor: pointer; width: 30px; height: 30px;';
		closeRevamp.innerHTML = '<img src="https://cache-ssl.celtra.com/api/static/v0508b455a6/runner/clazzes/CreativeUnit/close-up.svg" />';
		wrapOutstream.appendChild(closeRevamp);
	}
	var clRVMP = document.body.appendChild(wrapOutstream);
	closeRevamp.onclick = function(){ wrapOutstream.remove()};
	Revamp.setUpIMA();
  Revamp.checkAutoplaySupport();
	window.top.document.addEventListener("scroll", Revamp.playAds());
}

Revamp.setUpIMA = function(){
	this.videoContent = window.top.document.getElementById('contentElementRevamp');
	Revamp.createAdDisplayContainer();
	// Create ads loader.
	this.adsLoader = new google.ima.AdsLoader(this.adDisplayContainer);
	// Listen and respond to ads loaded and error events.
	this.adsLoader.addEventListener(
	  google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
	  Revamp.onAdsManagerLoaded, false);
	this.adsLoader.addEventListener(
	  google.ima.AdErrorEvent.Type.AD_ERROR,Revamp.onAdError, false);

	// An event listener to tell the SDK that our content video
	// is completed so the SDK can play any post-roll ads.
	var contentEndedListener = function() {
		this.adsLoader.contentComplete();
	};
	this.videoContent.addEventListener("ended",contentEndedListener);

	// Request video ads.
	var adsRequest = new google.ima.AdsRequest();
	adsRequest.adTagUrl = 'https://pubads.g.doubleclick.net/gampad/ads?iu=/4923229/Video_outstream_1x1&description_url=https%3A%2F%2Fwww.mundofauna.com%2F&tfcd=0&npa=0&sz=1x1%7C400x300%7C640x480&gdfp_req=1&output=vast&unviewed_position_start=1&env=vp&impl=s&correlator=';

	// Specify the linear and nonlinear slot sizes. This helps the SDK to
	// select the correct creative if multiple are returned.
	adsRequest.linearAdSlotWidth = 400;
	adsRequest.linearAdSlotHeight = 300;

	adsRequest.nonLinearAdSlotWidth = 400;
	adsRequest.nonLinearAdSlotHeight = 150;

	this.adsLoader.requestAds(adsRequest);
}
Revamp.checkAutoplaySupport = function() {
  var playPromise = this.videoContent.play();
  if (playPromise !== undefined) {
    playPromise.then(Revamp.onAutoplayWithSoundSuccess).catch(Revamp.onAutoplayWithSoundFail);
  }
}
Revamp.onAutoplayWithSoundSuccess = function() {
  this.videoContent.pause();
  this.autoplayAllowed = true;
  this.autoplayRequiresMuted = false;
  Revamp.autoplayChecksResolved();
}
Revamp.onAutoplayWithSoundFail = function() {
  Revamp.checkMutedAutoplaySupport();
}
Revamp.checkMutedAutoplaySupport = function() {
  this.videoContent.volume = 0;
  this.videoContent.muted = true;
  var playPromise = this.videoContent.play();
  if (playPromise !== undefined) {
    playPromise.then(Revamp.onMutedAutoplaySuccess).catch(Revamp.onMutedAutoplayFail);
  }
}
Revamp.onMutedAutoplaySuccess = function() {
  this.videoContent.pause();
  this.autoplayAllowed = true;
  this.autoplayRequiresMuted = true;
  Revamp.autoplayChecksResolved();
}
Revamp.onMutedAutoplayFail = function() {
  // Both muted and unmuted autoplay failed. Fall back to click to play.
  //this.videoContent.volume = 1;
  this.videoContent.muted = false;
  this.autoplayAllowed = false;
  this.autoplayRequiresMuted = false;
  Revamp.autoplayChecksResolved();
}
Revamp.autoplayChecksResolved = function(){
  var adsRequest = new google.ima.AdsRequest();
  adsRequest.adTagUrl = 'https://pubads.g.doubleclick.net/gampad/ads?iu=/4923229/Video_outstream_1x1&description_url=https%3A%2F%2Fwww.mundofauna.com%2F&tfcd=0&npa=0&sz=1x1%7C400x300%7C640x480&gdfp_req=1&output=vast&unviewed_position_start=1&env=vp&impl=s&correlator=';

  // Specify the linear and nonlinear slot sizes. This helps the SDK to
  // select the correct creative if multiple are returned.
  adsRequest.linearAdSlotWidth = 400;
  adsRequest.linearAdSlotHeight = 300;

  adsRequest.nonLinearAdSlotWidth = 400;
  adsRequest.nonLinearAdSlotHeight = 150;

  this.adsLoader.requestAds(adsRequest);
}
Revamp.createAdDisplayContainer = function() {
  // We assume the adContainer is the DOM id of the element that will house
  // the ads.
  var trp = window.top.document.getElementById('adContainerRevamp');
  this.adDisplayContainer = new google.ima.AdDisplayContainer(trp, this.videoContent);;
}
Revamp.playAds = function() {
  try {
    if (!this.adsInitialized) {
      this.adDisplayContainer.initialize();
      this.adsInitialized = true;
    }
    this.adsManager.init(640, 360, google.ima.ViewMode.NORMAL);
    adsManager.start();
  } catch (adError) {
    this.videoContent.play();
  }
}
Revamp.onAdsManagerLoaded = function(adsManagerLoadedEvent) {
  // Get the ads manager.
  var adsRenderingSettings = new google.ima.AdsRenderingSettings();
  adsRenderingSettings.restoreCustomPlaybackStateOnAdBreakComplete = true;
  // videoContent should be set to the content video element.
  this.adsManager =
      adsManagerLoadedEvent.getAdsManager(this.videoContent, adsRenderingSettings);

  // Add listeners to the required events.
  this.adsManager.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, Revamp.onAdError);
  this.adsManager.addEventListener(
      google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED, Revamp.onContentPauseRequested);
  this.adsManager.addEventListener(
      google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED,
      Revamp.onContentResumeRequested);
  this.adsManager.addEventListener(
      google.ima.AdEvent.Type.ALL_ADS_COMPLETED, Revamp.onAdEvent);

  // Listen to any additional events, if necessary.
  this.adsManager.addEventListener(google.ima.AdEvent.Type.LOADED, Revamp.onAdEvent);
  this.adsManager.addEventListener(google.ima.AdEvent.Type.STARTED, Revamp.onAdEvent);
  this.adsManager.addEventListener(google.ima.AdEvent.Type.COMPLETE, Revamp.onAdEvent);
  if (this.autoplayAllowed) {
    Revamp.playAds();
  } 
}

Revamp.onAdEvent = function(adEvent) {
  // Retrieve the ad from the event. Some events (e.g. ALL_ADS_COMPLETED)
  // don't have ad object associated.
  var ad = adEvent.getAd();
  switch (adEvent.type) {
    case google.ima.AdEvent.Type.LOADED:
      // This is the first event sent for an ad - it is possible to
      // determine whether the ad is a video ad or an overlay.
      if (!ad.isLinear()) {
        // Position AdDisplayContainer correctly for overlay.
        // Use ad.width and ad.height.
        this.videoContent.play();
      }
      break;
    case google.ima.AdEvent.Type.STARTED:
      // This event indicates the ad has started - the video player
      // can adjust the UI, for example display a pause button and
      // remaining time.
      if (ad.isLinear()) {
        // For a linear ad, a timer can be started to poll for
        // the remaining time.
        intervalTimer = setInterval(
            function() {
              var remainingTime = this.adsManager.getRemainingTime();
            },
            300);  // every 300ms
      }
      break;
    case google.ima.AdEvent.Type.COMPLETE:
      // This event indicates the ad has finished - the video player
      // can perform appropriate UI actions, such as removing the timer for
      // remaining time detection.
      if (ad.isLinear()) {
        clearInterval(intervalTimer);
      }
      break;
  }
}

Revamp.onAdError = function(adErrorEvent) {
  // Handle the error logging.
  console.log(adErrorEvent.getError());
  this.adsManager.destroy();
}

Revamp.onContentPauseRequested = function() {
  this.videoContent.pause();
  // This function is where you should setup UI for showing ads (e.g.
  // display ad timer countdown, disable seeking etc.)
  // setupUIForAds();
}

Revamp.onContentResumeRequested = function() {
  this.videoContent.play();
  // This function is where you should ensure that your UI is ready
  // to play content. It is the responsibility of the Publisher to
  // implement this function when necessary.
  // setupUIForContent();
}
Revamp.getOffset = function(el) {
	const rect = el.getBoundingClientRect();
	return {
	  left: rect.left + window.scrollX,
	  top: rect.top + window.scrollY
	};
  }