<?php

/**
 * Load parent theme style
 */
add_action( 'wp_enqueue_scripts', 'jnews_child_enqueue_parent_style' );

function jnews_child_enqueue_parent_style()
{
    wp_enqueue_style( 'jnews-parent-style', get_parent_theme_file_uri('/style.css'));
}
function get_post_primary_category( $post = 0, $taxonomy = 'category' ){
    if ( ! $post ) {
        $post = get_the_ID();
    }

    $terms        = get_the_terms( $post, $taxonomy );
    $primary_term = array();

    if ( $terms ) {
        $term_display = '';
        $term_slug    = '';
        $term_link    = '';
        if ( class_exists( 'WPSEO_Primary_Term' ) ) {
            $wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term               = get_term( $wpseo_primary_term );
            if ( is_wp_error( $term ) ) {
                $term_display = $terms[0]->name;
                $term_slug    = $terms[0]->slug;
                $term_link    = get_term_link( $terms[0]->term_id );
            } else {
                $term_display = $term->name;
                $term_slug    = $term->slug;
                $term_link    = get_term_link( $term->term_id );
            }
        } else {
            $term_display = $terms[0]->name;
            $term_slug    = $terms[0]->slug;
            $term_link    = get_term_link( $terms[0]->term_id );
        }
        $primary_term['url']   = $term_link;
        $primary_term['slug']  = $term_slug;
        $primary_term['title'] = $term_display;
    }
    return $primary_term;
}

use Facebook\InstantArticles\Elements\Ad;
add_filter('instant_articles_content','custom_ad_content',10,1);
function custom_ad_content($content){
    $id = get_the_id();
    $vdjsd = get_post_meta( $id, 'video_destacado', true );
    $vdjsr = get_post_meta( $id, 'video_recomendado', true );
    if(!empty($vdjsd)){
        $videjod = '<iframe frameborder="0" width="300" height="300" src="https://www.dailymotion.com/embed/video/'.$vdjsd.'?ads_params=site%3Di24_rs&sharing-enable=false&queue-enable=false&queue-autoplay-next=false&quality=380&autoplay=true&mute=true"></iframe>';
        $content = $videjod.$content;
    }
    if(!empty($vdjsr)){
        $videjor = '<iframe frameborder="0" width="300" height="300" src="https://www.dailymotion.com/embed/video/'.$vdjsr.'?ads_params=site%3Di24_rs&sharing-enable=false&queue-enable=false&queue-autoplay-next=false&quality=380&autoplay=true&mute=true"></iframe>';
        $content = $content.$videjor;
    }
    return $content;
}
add_shortcode('videoDLM', function($atts){
    $html = '';
    $idDLM = $atts['id'];
    $typeDLM = $atts['type'];
    if(!empty($idDLM)){
        $html .= '<div class="video_wrapper_responsive vd-'.$typeDLM.'"><div id="'.$typeDLM.'-DLM" data-id="'.$idDLM.'"></div></div>';
    wp_enqueue_script('DMapi', 'https://api.dmcdn.net/all.js');
    }  
    return $html;
});
add_shortcode('playlistHome',function(){
    $html = '';
    $urlp = 'https://api.dailymotion.com/videos?fields=title%2Cid%2Cthumbnail_360_url&owners=x2jybk1&limit=3&tags=recomendado';
    $html .= '<div class="dm_video_list">';
    $dataDM = file_get_contents($urlp);
    $playlist = json_decode($dataDM);
    foreach ($playlist->list as $index => $video) {
        $videoDMp = $video->id;
        $titleDMp = $video->title;
        $imageDMp = $video->thumbnail_360_url;
        if($index == 0){
            $html .= '<div class="dm_video_content">
                <div class="dm_video_holder">
                    <div class="dm_video_container">
                    <iframe src="https://www.dailymotion.com/embed/video/'.$videoDMp.'?ads_params=category%253Dhome%2526site%3Destrending&queue-autoplay-next=false&queue-enable=false&sharing-enable=false" width="100%" height="100%" frameborder="0" allowfullscreen="true"></iframe>
                    </div>
                </div>
            </div>';
            $html .= '<div class="dm_side_content">';
            $html .= '<div class="dm_item_side currentdm" data-id="'.$videoDMp .'">
                <div class="dm_thumb_video">
                    <img src="https://www.estrending.com/wp-content/themes/jnews/assets/img/jeg-empty.png" data-src="'.$imageDMp.'" class="wp-post-image lazyload" />
                </div>
                <div class="dm_title_video">
                    <h3>'.$titleDMp.'</h3>
                </div>
            </div>';
        }else{
            $html .= '<div class="dm_item_side" data-id="'.$videoDMp .'">
                <div class="dm_thumb_video">
                    <img src="https://www.estrending.com/wp-content/themes/jnews/assets/img/jeg-empty.png" data-src="'.$imageDMp.'" class="wp-post-image lazyload" />
                </div>
                <div class="dm_title_video">
                    <h3>'.$titleDMp.'</h3>
                </div>
            </div>';
        }
    }
    $html .= '</div>';
    $html .= '</div>';
    return $html;
});

//Allow disable Facebook Instant Article

add_filter("instant_articles_should_submit_post", "ad_validation_FBIA",10, 2);
function ad_validation_FBIA($should_show, $IA_object){
    $validateIA = get_post_meta($IA_object->get_the_id(), "disable_ia");
    if($validateIA){
        return false;
    }else{
        return true;
    }   
}
add_action('post_submitbox_misc_actions', 'create_disable_ia');
add_action('save_post', 'save_disable_ia');
function create_disable_ia(){
    $post_id = get_the_ID();
  
    if (get_post_type($post_id) != 'post') {
        return;
    }
  
    $value = get_post_meta($post_id, 'disable_ia', true);
    wp_nonce_field('ad_disable_ia_nonce_'.$post_id, 'ad_disable_ia_nonce');
    ?>
    <div class="misc-pub-section misc-pub-section-last">
        <label><input type="checkbox" value="1" <?php checked($value, true, true); ?> name="disable_ia" /><?php _e('Disable this post in FB-IA', 'pmg'); ?></label>
    </div>
    <?php
}
function save_disable_ia($post_id){
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    
    if (
        !isset($_POST['ad_disable_ia_nonce']) ||
        !wp_verify_nonce($_POST['ad_disable_ia_nonce'], 'ad_disable_ia_nonce_'.$post_id)
    ) {
        return;
    }
    
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }
    if (isset($_POST['disable_ia'])) {
        update_post_meta($post_id, 'disable_ia', $_POST['disable_ia']);
    } else {
        delete_post_meta($post_id, 'disable_ia');
    }
}
function user_can_richedit_custom() {
    global $wp_rich_edit;
    if (get_user_option('rich_editing') == 'true' || !is_user_logged_in()) {
        $wp_rich_edit = true;
        return true;
    }
    $wp_rich_edit = false;
    return false;
}

add_filter('user_can_richedit', 'user_can_richedit_custom');

function videos_content( $content ) {
    if ( is_single() && 'post' == get_post_type() ) {

        $custom_content = "";

        $custom_content .= $content;
        $live = false;
        $video_DLM = get_post_meta( get_the_ID(), 'video_recomendado', true );
        
        $post_tags = get_the_tags( get_the_ID());
        if ( function_exists( 'ampforwp_is_amp_endpoint' ) && ampforwp_is_amp_endpoint() ) {
            if(!empty($v_DLM)){
                $precontent = '<amp-dailymotion data-videoid="'.$v_DLM.'" autoplay data-mute="true" layout="responsive" width="480" height="270"></amp-dailymotion></br>';
                $custom_content = $precontent.$content;
            }
            if(!empty($video_DLM)){
                $custom_content .= '<strong>Imperdible</strong><br />';
                $custom_content .= '<amp-dailymotion data-videoid="'.$video_DLM.'" autoplay data-mute="true" layout="responsive" width="480" height="270"></amp-dailymotion>';
                
            }
        }else{
            if( has_tag( 'branded' ) ) {
            }else{
            if(!empty($video_DLM)){
                $custom_content .= '<div class="group_video_r"><strong>Imperdible</strong><br />';
                $custom_content .= do_shortcode('[videoDLM id="'.$video_DLM.'" type="recomendado" title="'.$tt_recomendado.'"]');
                $custom_content .= '</div>';
                }
            }
        }
        return $custom_content;
    } else {
        return $content;
    }
}
add_filter( 'the_content', 'videos_content' );

add_action( 'init', function(){
    wp_embed_register_handler( 
        'myig', 
        '/https?\:\/\/(?:www.)?instagram.com\/p\/(.+)/',   // <-- Adjust this to your needs!
        'ig_embed_handler' 
    );
} );
//clean ig embed
function ig_embed_handler( $matches, $attr, $url, $rawattr ){
    $idIG = $matches[1];
    if(!empty($idIG)){
        if(strpos($idIG, 'photo') !== false){
            $idIG = str_replace('photo/', '', $idIG);
            $baseurl = 'https://graph.facebook.com/v10.0/instagram_oembed?url=instagram.com/p/'.$idIG.'&fields=thumbnail_url,author_name&access_token=757874017927860|9eae7061d9a8d7177a64e84ff2a45bb2';
            $dataDIG = file_get_contents($baseurl);
            $igEmbed = json_decode($dataDIG,true);
            $imgIG = $igEmbed["thumbnail_url"];
            $authorIG = $igEmbed["author_name"];
            if(!empty($imgIG)){
                $html = '<figure style="width: 100%" class="wp-caption"><a href="https://www.instagram.com/p/'.$idIG.'"><img src="'.$imgIG.'" alt=""></a><figcaption class="wp-caption-text"><a href="https://www.instagram.com/'.$authorIG.'", target="_blank" rel="noopener">Fuente: instagram/'.$authorIG.' </a></figcaption></figure>';
                return $html;
            }
        }else{
            $baseurl = 'https://graph.facebook.com/v10.0/instagram_oembed?url=instagram.com/p/'.$idIG.'&fields=html&access_token=446337026596225|6ecffb63a90e3fa01434225706abd2fd';
            $dataDIG = file_get_contents($baseurl);
            $igEmbed = json_decode($dataDIG,true);
            $embedIG = $igEmbed["html"];
            if(!empty($embedIG)){
                $newhtml = strip_tags($embedIG, '<blockquote><div><figure><p><a><script>');
                $newhtml = '<div class="embed">'.$newhtml.'</div>';
                return $newhtml;
            }
        }
    }
    
}

function appAD_fetch_embed( $cached_html, $url, $attr, $post_id ) {
    if ( false !== strpos( $url, "://twitter.com")){
        $pattern  = '#https?://twitter\.com/(?:\#!/)?(\w+)/status(es)?/(\d+)#is';
        preg_match( $pattern, $url, $matches );
        $urlt = $matches[0];
        $urltw = 'https://twitframe.com/show?url='.$urlt;
        $cached_html = '<div class="embed" style="max-width:500px;"><iframe border="0" frameborder="0" height="410" width="100%" src="'.$urltw.'"></iframe></div>';
    }
    return $cached_html;
}
add_filter('embed_oembed_html','appAD_fetch_embed', 10, 4 );

function custom_embed_settings($code){
    if(strpos($code, 'dai.ly') !== false || strpos($code, 'dailymotion.com') !== false){
        $return = preg_replace("@src=(['\"])?([^'\">\s]*)@", "src=$1$2?queue-autoplay-next=false&queue-enable=false&sharing-enable=false", $code);
        return '<div class="video_wrapper_responsive embed">'.$return.'</div>';
    }
    if(strpos($code, 'tiktok.com') !== false){
        return '<div class="embed">'.$code.'</div>';
    }
    return $code;

}

add_filter('embed_handler_html', 'custom_embed_settings');
add_filter('embed_oembed_html', 'custom_embed_settings');

add_action('rest_api_init', function () {
    $ad_imgft_schema = array(
        'description'   => 'Image post featured',
        'type'          => 'string',
        'context'       =>   array( 'view' )
    );
    register_rest_field(
        'post',
        'imgft_field',
        array(
            'get_callback'      => 'oi_image_featured',
            'update_callback'   => null,
            'schema'            => $ad_imgft_schema
        )
    );
    $ad_videod_schema = array(
        'description'   => 'Featured video DM',
        'type'          => 'string',
        'context'       =>   array( 'view' )
    );
    register_rest_field(
        'post',
        'videod',
        array(
            'get_callback'      => 'oi_videod',
            'update_callback'   => null,
            'schema'            => $ad_videod_schema
        )
    );
    $ad_videord_schema = array(
        'description'   => 'Related video DM',
        'type'          => 'string',
        'context'       =>   array( 'view' )
    );
    register_rest_field(
        'post',
        'videord',
        array(
            'get_callback'      => 'oi_videord',
            'update_callback'   => null,
            'schema'            => $ad_videord_schema
        )
    );
});
function oi_image_featured( $object, $field_name, $request ) {
    return get_the_post_thumbnail_url( $object['id'], 'full' );
}
function oi_videod( $object, $field_name, $request ) {
    $dm = get_post_meta($object['id'],'video_destacado',true);
    if(!empty($dm)){
        $dmpid = $dm;
    }
    return $dmpid;
}
function oi_videord( $object, $field_name, $request ) {
    $dm = get_post_meta($object['id'],'video_recomendado',true);
    if(!empty($dm)){
        $dmpid = $dm;
    }
    return $dmpid;
}

add_action('wp_head', 'adOPs_calls',10);
function adOPs_calls(){
    $adTagCall = '';
    $typep = '';
    $varcontrol = 1;
    if(is_single()){
        if(has_tag('noads') or has_tag('branded')){
            $varcontrol  = 0;
        }
        $tempID = get_the_ID();
        $prm_ct = get_post_primary_category($tempID, 'category');
        if(!empty($prm_ct)){
            $temp_section = $prm_ct['slug'];
            $section = $temp_section;

        }
        $typep = 'nodes';
    }
    if(is_page()){
        $varcontrol = 1;
        $typep = 'page';
    }
    if($varcontrol == 1){
        $tags = '<!--'.$varcontrol.'--><script>';
        $tags .= '(function($) {
        var sizesScroll = [[728, 90],[970, 90]];
        var sizesTop = [[728, 90],[970, 90]];
        var sizesMid = [[728, 90],[728,250]];
        var sizesSqr1 = [300,250];
        var sizesSqr6 = [[728, 90],[970, 90]];
        var sizesFoot = [[728, 90],[970, 90],[1,1]];
        if ($(window).width() < 720) {
            sizesScroll = [320,50];
            sizesTop = [[320,100],[320,50]];
            sizesMid = [[320,50],[300,250],[300,600]];
            sizesSqr1 = [[320, 100],[320, 50],[300, 250]];
            sizesSqr6 = [[320, 100],[320, 50],[300, 250]];
            sizesFoot = [[320, 50],[1,1]];
        }
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() { '; 
        $tags .= 'googletag.defineSlot("/22167442/scrollbanner_mid", sizesScroll, "scrollbanner").addService(googletag.pubads());
        googletag.defineSlot("/22167442/top_banner_atf", sizesTop, "topbanner").addService(googletag.pubads());
        googletag.defineSlot("/22167442/midbanner_mid", sizesMid, "midbanner").addService(googletag.pubads());
        googletag.defineSlot("/22167442/sqrbanner1_atf", sizesSqr1, "sqrbanner1").addService(googletag.pubads());
        googletag.defineSlot("/22167442/sqrbanner6_mid", sizesSqr6, "sqrbanner6").addService(googletag.pubads());
        googletag.defineSlot("/22167442/footbanner_btf", sizesFoot, "footbanner").addService(googletag.pubads());
        googletag.defineSlot("/22167442/native_overlayer",[1,1], "overlay_1x1").addService(googletag.pubads());';
        if(is_single()){
            $tags .= 'googletag.defineSlot("/22167442/native_intext",[1,1], "intext_1x1").addService(googletag.pubads());';
        }
        if(is_front_page()){
            $typep = 'home';    
        }
        if(is_category()){
            $catSite = get_the_category();
            if ( ! empty( $catSite ) ) {
                $temp_section  = $catSite[0]->slug; 
                $section = $temp_section;
            }
            $typep = 'category';    
        }
        $tags .= 'googletag.pubads().setTargeting("site", "mundofauna");';
        if(is_front_page()){
            $tags .= 'googletag.pubads().setTargeting("tipo_de_pag", "home");';
        }else{
            $tags .= 'googletag.pubads().setTargeting("tipo_de_pag", "'.$typep.'");';
            $tags .= 'googletag.pubads().setTargeting("category", "'.$section.'");';
        }
        $tags .= 'googletag.pubads().collapseEmptyDivs();';
        $tags .= 'var SECONDS_TO_WAIT_AFTER_VIEWABILITY = 30;
        googletag.pubads().addEventListener("impressionViewable", function(event) {
            var slot = event.slot;
            if (slot.getTargeting("refresh").indexOf("true") > -1) {
            setTimeout(function() {
                googletag.pubads().refresh([slot]);
            }, SECONDS_TO_WAIT_AFTER_VIEWABILITY * 1000);
            }
        });
        googletag.pubads().addEventListener("slotRenderEnded", function(event) {
            if (event.slot.getSlotElementId() == "footbanner") {
                $("#wrap_footer_banner").fadeIn("fast");  
            }
        });';
        $tags .= 'googletag.enableServices(); }); })(jQuery);';
        $tags .= '</script>';
        echo $tags;
    }
}